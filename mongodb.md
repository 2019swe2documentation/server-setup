# MongoDB Configuration
[Back to Server Config](readme.md)

MongoDB is a fast, and modern NoSQL database that allows greater flexibility, and is quicker to implement than SQL based databases. MongoDB useage has exploded over the last few years as it is easy to implement and scales very well from the smallest app, to the complex biggest system.

## Installing MongoDB

Before starting, be sure to familiarize yourself with the Ubuntu command line, and login to your droplet via SSH.


We will be using MongoDB's public package repo to install Mongo and all of its dependencies. This is a mostly automated task, but requires some setup first.


1. Make sure to follow **ALL** instructions under Installing Packages section of Readme.md before going forward. You should already have MongoDB installed. 
2. Verify that MongoDB is installed by running
        ```
        mongod --version | grep 'db version'
        ```
   from an SSH shell. Make sure it says 4.2 or newer, otherwise you may have installed an older version.
   
3. Hold the current MongoDB Version
    Because MongoDB releases new versions often, we want to hold on the installed version so we don't have any unintended upgrades.

        ```
        echo "mongodb-org hold" | sudo dpkg --set-selections
        echo "mongodb-org-server hold" | sudo dpkg --set-selections
        echo "mongodb-org-shell hold" | sudo dpkg --set-selections
        echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
        echo "mongodb-org-tools hold" | sudo dpkg --set-selections
        ```

4. Make sure MongoDB starts automatically
        ```
        sudo systemctl enable mongod
        ```
## Starting MongoDB

### To Start the mongod service
```
sudo systemctl start mongod
```

### To verify mongod is running
```
sudo systemctl status mongod
```

And look for **\[initandlisten\] waiting for connections on port 27017**


You can now connect to mongo shell using:
```
mongo
```

MongoDB is now running, but without any authorization. The following steps are NOT necesarrily stable.

## Securing MongoDB (DON'T DO THIS UNLESS YOU EXPECT PROBLEMS!)

1. Connect to mongo shell using `mongo`
2. Switch to the admin database
```
use admin
```

3. Create an admin user
```
db.createUser(
  {
    user: "myUserAdmin",
    pwd: "abc123",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)
```

4. Create a 'normal' user for DB access
```
db.createUser(
  {
    user: "myUserAdmin",
    pwd: "abc123",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)
```

5. Verify Your user exists
```
show users
```

6. Exit mongo using `exit`

7. Enable Authorization in the MongoDB config
        1. Open mongod.conf using
        ```
        sudo nano /etc/mongod.conf
        ```
        2. Remove the # in front of **security**

        3. Under the security: tab add:
                authorization: enabled

        It should look like:
        ```
        security:
        authorization: enabled
        ```

        Control + O to save, then Control + X to exit.

8. Now restart mongod with `sudo service mongod restart`

9. Now whenever you connect to MongoDB use
```
mongo -u <username> -p <password> --authenticationDatabase "admin"
```
10. You can repeat the above steps again to create an unprivileged user.

11. Remember to provide the username and password when using mongoose as well.
-->
We are done setting up MongoDB for now. We will explore some more with Mongo later in the docs.

We can use the mongo shell to manage data or even better why not connect using [NodeJS](nodejs.md)?
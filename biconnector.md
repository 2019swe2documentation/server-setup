# MongoBI Connector and Tableau
## Background Information
The connector should be installed on your server, and will act as a relay between Tableau and your MongoDB instance. Tableau is only able to understand relational database schemas, and the MongoDB BI Connector is able to translate the document based schemas from MongoDB to relational schemas that can be understood by Tableau.

The connector comes with 3 applications, and a sample config.yml file that can be used to set up all sorts of options.

* `mongodrdl` - This is able to analyze a MongoDB collection, and form a relational schema for it. The schema can be then stored in a file, or in a collection in mongodb. (not directly covered in this guide)
* `mongosqld` - This actually runs the server endpoint that connects Tableau and MongoDB. 
* `mongotranslate` (not used in this guide)

## Setting up the Connector

Forewarning: This guide only covers the installation of the mongoDB BI Connector on a server. Tableau configuration is not discussed in this guide at this time, and must be done according to the instructions on their site, including installing any drivers required. 

In this repository, there is a config file that should work with each team's mongoDB instance, once we setup a few things. First we need to download and unpack the connector, and then add in the appropriate config files, and finally start the service.

### Download and Unpack the Connector, and install to /usr/bin

The commands below will download the archive file of the mongoDB connector, unpack it, and copy it to `/usr/bin`, effectively installing it for systemwide use.
```
wget https://info-mongodb-com.s3.amazonaws.com/mongodb-bi/v2/mongodb-bi-linux-x86_64-ubuntu1604-v2.12.0.tgz
gunzip mongodb-bi-linux-x86_64-ubuntu1604-v2.12.0.tgz
tar -xvf mongodb-bi-linux-x86_64-ubuntu1604-v2.12.0.tar
sudo cp mongodb-bi-linux-x86_64-ubuntu1604-v2.12.0/bin/* /usr/bin
```

We also need to make some folders before we get to the next step:
```
sudo mkdir /etc/mongosqld/
sudo mkdir /var/log/mongosqld/
```

Next download the two files in the "mongodb" folder in this repository, and place them in the following locations on your server:
* config.yml -> `/etc/mongosqld/config.yml`
* mongosqld.service -> `/etc/systemd/system/mongosqld.service`

This creates the config.yml file that mongosqld (the connector) will use, and a systemd service that will run the connector in the background perpetually.

Finally, we need to start the service, check that it works, and enable the service to start after reboots.
```
sudo systemctl start mongosqld
systemctl status mongosqld
```
If you got all greens, you're good to go. Otherwise, check `sudo journalctl -xe` for details on any failures you may have received. If all was successful:
```
sudo systemctl enable mongosqld
```

## Connecting to MongoDB

If the service is running correctly, then clients can connect at `[server-ip]:3307`

## Additional Information/Documentation

* https://docs.mongodb.com/bi-connector/master/reference/mongosqld/
* https://docs.mongodb.com/bi-connector/master/reference/mongodrdl/ (not directly covered in this guide)
# Server Configuration Guide

This guide is written for setting up droplets on Digital Ocean for Software Engineering 2 at St. Edward's University for Fall 2019. While there is a lot of information here, it is very important that you complete EVERY step in order to get a stable, and fully functional environment. Make sure to continue on to the MongoDB, NodeJS and Nginx pages as they have additional setup information.

## Software Versions:

 * nginx: 1.14.0/Ubuntu
 * Ubuntu: 18.04 x64
 * MongoDB: v4.2
 * NodeJS: v10.16.3
 * certbot: 0.31.0
 * npm: 6.11.2

Program versions can be checked by calling `[program-name] --version`, or if that doesn't return it, `[program-name] -v`.

## Important Definitions And Terms

 * `~` : This character refers to the current signed-in user's home directory on Linux/UNIX machines (e.g. if the current signed in user is `team23`, `~` is the same as `/home/team23`). This also works on Windows using Powershell, but not the Command Prompt (`cmd.exe`), and is recognixed on Macs. All of our servers for this course use Ubuntu (a Linux distribution) so the `~` is a valid way to get the user's home directory. In Powershell, `~` refers to your user home directory, which is `C:\Users\[your-username]`.
 * `pwd` : pwd is the UNIX command for "present working directory". Calling this command will print out where you currently are in the system (useful if you think a file or folder should be there but you aren't sure where you are).
 * `mv [source-file-or-directory] [destination-file-or-directory]` : mv is the unix command for "move". It takes a source and a destination, and is also used to rename a file. Passing just a file name or directory name wihout a full path will implicitly use the current path that the terminal is at (which can be shown with `pwd`).
 * `cd [directory]` : cd is the unix command for "change directory". This changes the current directory your terminal is looking at to the directory specified. This command can take an absolute path which starts with either `/` or `~` (e.g. `cd /etc/nginx/` or `~/.ssh/`) or can take a path *relative to the directory you are currently in* (e.g. you are in a directory with a subdirectory called "supply", you can say `cd supply` and you will descend into that folder). 
 * `sudo` : sudo stands for "switch user do". This command allows for executing single commands as a user with elevated permissions on the system, without being logged into that elevated account all the time.
 * `apt` : The package manager on Ubuntu. This is the command for installing/removing/updating software and applications on Ubuntu.

## Creating a New SSH Key

For connecting to the server this guide will walk you through setting up, we first need to generate a new SSH Key. In order to generate a key on a Mac, Linux box, or a PC running a recent version of Windows 10, the process is relatively straightforward. We will be creating an RSA keypair (RSA is one of tghe standard encryption algorithms for public/private key encryption, and it is widely accepted).

Use the following terminal/prompt according to your system type below:

 - MacOS: `Terminal.app`, found in Applications/ folder
 - Windows: `Powershell`, openable using `Windows + R` keys on the keyboard, and typing 'powershell' in the dialog box that appears and hitting enter
 - Linux: Your terminal emulator of choice (ensure that you have ssh packages installed, if you'ure using a Linux distribution as your primary OS then you should be able to do this)

 To generate a new RSA keypair and save it in `~/.ssh/`, run the following command: 

***Important Note: Make sure to sub out the filename of the ssh key for something other than id_rsa if you already have other keys on your system in ~/.ssh!!!***

```
 ssh-keygen -t rsa -f ~/.ssh/id_rsa -b 2048
```

*Note: if the above fails due to the `~/.ssh` directory not existing, run the following, and then attempt to generate the key again.*

```
mkdir -p ~/.ssh/
```

It will then ask you for a passphrase to use alongside the key, but we can leave this blank. To leave the passphrase blank press enter twice on the passphrase dialogs, and your private-key will be created and saved at `~/.ssh/id_rsa`. Your public key will be saved at `~/.ssh/id_rsa.pub`. Your private key should never leave your machine (unless shared out to other members of your team). Your public key is what goes on remote machines such as Digital Ocean, Bitbucket, and your server. When logging into any of these services via SSH, your machine and the remote machine will authenticate using your private key saved on your machine, and your public key saved on the remote machine.  

## Creating a New Droplet via Digital Ocean WebUI

To work with the rest of this guide, we need a sever with which we can manipulate. Digital Ocean provides these virtual servers for us, known in their terms as "Droplets". To spin up a new Ubuntu 18.04x64 droplet, walk though the following steps: 

 1. Login via your Digital Ocean account after receiving an invite to the Team Account
 2. Click on your account image in the top right hand corner, and click on the team account (shown under your own)
 3. In the top right, click the green "Create" button, and select "Droplets" from the dropdown
 4. Under "Choose an Image", make sure that Ubuntu is selected (outlined in blue), and click the dropdown under Ubuntu and select "18.04 x64"
 5. Under "Choose a Plan", ensure "Standard" is selected, and on the carousel of server options click the Left arrow (next to the $40/month droplet configuration) and choose the $5/month configuration
 6. Do not enable backups
 7. You can skip the "Block Storage" section
 8. In "Choose a Datacenter Region", you may select any datacenter but I personally would choose San Francisco as it is the closest geographical region to St. Edward's (A good thing, reduces roundtrip time for requests)
 9. In "Select Additional Options", ensure "Monitoring" is checked. This will allow the display of CPU/Memory/Resource graphs on the Digital Ocean dashboard later on
 10. In "Add your SSH Keys", click the "New SSH Key" button. Copy the contents of your `~/.ssh/id_rsa.pub` file that we created in the last section (open the file, and copy the contents with your text editor), and paste it into the "SSH Key Content" field. Give your key a name, and hit the "Add SSH Key" button at the bottom of the window.
 11. Make sure the SSH key you just added to Digital Ocean is checked in the "Add your SSH Keys" section of the creation page.
 12. Finally, select the number of droplets you wish to create (1 for now), and change the hostname of your droplet to something descriptive (i.e. TEAMXX-[droplet_name])
 13. Click "Create" at the bottom of the page

![](images/droplet-1.png)
![](images/droplet-2.png)
![](images/droplet-3.png)
![](images/droplet-4.png)
![](images/droplet-5.png)
![](images/droplet-6.png)
![](images/droplet-7.png)
![](images/droplet-8.png)

You will now be redirected to the Digital Ocean Dashboard, and you will see the droplet you just created appear under the list of droplets. It will take up to 30 seconds to provision and create your droplet on the Digital Ocean infrastructure, and once the droplet is ready the progress bar will complete and you will see your droplet's IPv4 address (and you can click on the droplet to see all of the manipulation options as well as access the network/cpu/memory statistics for the droplet).

## Domain Routing in Digital Ocean

#### Quick Intro to Structure of a Domain
A full domain name for a server (Fully Qualified Domain Name, FQDN) is structured with 3 distinct sections:

`[subdomains].domain.tld`

 * `subdomains`: subdomain examples: `www.`, `team25.demand`, etc. This is what we will be working on in this section, and is configured via DNS records on a domain. You can have multiple subdomains for a domain, and a subdomain can have multiple parts.
 * `domain.tld`: This is the domain you purchase from a registrar/is given to you. Example: `google.com`, `softwareengineeringii.com`

#### Configuring DNS Records in Digital Ocean

Now that we have a server that we can work with, we need to set up a domain to route to it so we can access the server without using an ip address. To configure a new subdomain to route to the server we just created, use the following steps:

 1. Click to expand the "Manage" on the left column of the window (The blue column), then click on "Networking"
 2. Under the "Domains" tab (default), you can either add a new domain, or use the existing `softwareengineeringii.com` domain already set up on the team account. To use the `softwareengineeringii.com` domain, click on it from the "Domains" list.
 3. For regular web request routing (which is what we are using for web requests) we are going to create an `A` record on the domain associated to our server's IP address. Make sure the `A` tab is selected under "Create new record".
 4.  In the "Hostname" field, enter the subdomain you wish to use for your server (i.e. `software2-test`). This must be unique for all of the other `A` records on the domain.
 5. In the "Will Direct To" field, clicking on it will give a list of droplets associated with the account. Click the droplet you created in the section above.
 6. Now press "Create Record" on the right (you can leave the "TTL" as is).

![](images/domain-1.png)
![](images/domain-2.png)
![](images/domain-3.png)

After a little bit of time for the DNS system to update with the changes you just made, the subdomain on the domain you picked will route requests to your server, and we can take advantage of that later on in the Nginx section.

## SSH into Server

The first time you go to ssh into a new Digital Ocean server, the only default user is the `root` user, so that is who we shall sign in as for the purpose of this guide. You should create other accounts and disable logging in as `root` as a security best practice, but that is outside the scope of this guide and will come later on. This guide makes no assumptions that you are running as a user without elevated privelages. 

SSH tutorials are defined below, based on your operating system. If you are using a recent version of Windows 10 (1803 or later, checkable in Settings>System>About this PC), your machine already has OpenSSH installed by default. If you are on an older version of Windows, see the section on using PuTTY below.

### Using OpenSSH (MacOS, Windows 10 Version 1803+, Linux)

We will be connecting to the server using the key-pair we created at the beginning of this guide. Digital Ocean handled adding the key to the droplet for us, so we are good to connect in using the key, our `root` user, and our domain name that we set up in the last section. The ssh command should be formatted as follows, and should be run from the same terminal as used for `ssh-keygen`.  

 - MacOS: `Terminal.app`, found in Applications/ folder
 - Windows: `Powershell`, openable using `Windows + R` keys on the keyboard, and typing 'powershell' in the dialog box that appears and hitting enter
 - Linux: Your terminal emulator of choice (ensure that you have ssh packages installed, if you'ure using a Linux distribution as your primary OS then you should be able to do this)

```
ssh -i ~/.ssh/id_rsa root@[domain-name-or-ip]
```

On your first SSH connection, the system will ask if you'd like to accept the authenticity of the host. Say yes to this

*Reminder: The key generated above defaults to* `id_rsa`*, however if you are using a different keypath on your machine substitude in that path for* `~/.ssh/id_rsa`.

### Using PuTTY (Windows 10 Below Version 18.03)

*** Coming soon ***


## Updating and Installing Packages on a Newly Created Server

If you succesfully logged into the server for the first time, it is time to update the packages and package repositories (lists of packages that can be installed on the system) to the most recent version using the following commands, and add more repositories we will need on the system. Make sure to do these one at a time (the last line is really 2 commands, strung together with `&&`. This will run both commands back to back). You may receive a couple popups in the terminal that asks if you would like to overwrite a file. If so, choose "keep the local version currently installed".

```
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
sudo add-apt-repository universe
sudo apt-add-repository ppa:certbot/certbot --yes
sudo apt update && sudo apt upgrade --yes
```

Now it is time to install the rest of the applications and middleware needed to complete the server configuration, as well as remove any previously installed packages that are installed but no longer needed.

```
sudo apt install nginx-full certbot python-certbot-nginx mongodb-org --yes
sudo apt autoremove --yes
```

`NodeJS` and `npm` installation and configuration can be found in the [NodeJS Documentation](nodejs.md). 

## MongoDB Setup

* [MongoDB Docs](mongodb.md)

## NodeJS Setup

* [NodeJS Docs](nodejs.md)

## Nginx Server Configuration

* [Nginx Docs](nginx.md)

After all finishing all steps above, you can go ahead and test your site by going back to the Node project directory, and starting the app in your terminal. After the app is started, heading to the domain you set up earlier in this guide will show the same page as going to [droplet-ip]:3000.

```
cd ~/SWEII-Node-Demo
npm start
```


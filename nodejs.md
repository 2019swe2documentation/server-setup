# Node JS
[Back to Server Config](readme.md)

Be sure to follow **ALL** instructions on previous pages before going forward.

## Introduction
We will be using NodeJS for our server side logic and APIs. NodeJS is a fast, scalable server side programming language based on JavaScript and Google's V8 Javascript Engine. 


Before we dive too far into development with Node, we need to familiarize with some key concepts. Node can be overwhelming without proper understanding of the terminoligy, and knowledge of some basic terms and concepts. This doc is not meant to be a replacement for outside resources, and is merely a starting point into NodeJS.

## Node Terms
First the figurative skeleton of a Node app is the **package.json** file. This file contains the name, version number, **dependencies**, **scripts** and many other configurations for your project. While Node will run without one, a package.json file is necessary for anything more advanced than hello world.


Node fully embraces modularity and code reuse. Because of this there is a large community of packages available to use in your projects, which you can browse at npmjs.com. These packages are called dependencies, and are available to install through the npm package manager. npm is one of the most useful tools in dealing with Node projects, and has a lot of useful tools beyond just package management, but those are out of the scope of this doc.

We will focus on these npm commands:

Usage:

```
npm <command>
```

* start: Runs the start script for your project, defined in package.json
* run <arg>: Runs the specified script in package.json
* install: Installs all the dependencies for your project defined in the package.json into the **node_modules** directory
* test: runs defined tests on your project
* update: Updates all installed packages to the newest public version. (THIS CAN BE DANGEROUS!)

## Installing Node

### To install NodeJS:

```
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```

### Verify Node is installed:

```
node --version
```

## Setting up the demo app
Finally lets get to actually setting up a NodeJS demo app!

* Clone the Demo Project

```
git clone https://github.com/jacobacon/SWEII-Node-Demo.git
```

* CD into the project directory

```
cd SWEII-Node-Demo
```

* Install Dependencies

```
npm install
```

* Run the Project

```
npm start
```

* The app is now listening on port 3000 for requests. You can use a browser to open <Digital Ocean Droplet IP>:3000 to view the demo!

To stop the application, press `Ctrl + C` for now. We will revist and relaunch the Node application after we configure our webserver to use Node, since we don't want to manually specify port 3000 every time we make a request. [Nginx Configuration here.](nginx.md)